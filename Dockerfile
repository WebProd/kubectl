FROM bitnami/minideb:stretch
LABEL maintainer "Bitnami <containers@bitnami.com>"

# Install required system packages and dependencies
RUN install_packages ca-certificates wget python3 python3-pip
RUN wget -nc -P /tmp/bitnami/pkg/cache/ https://downloads.bitnami.com/files/stacksmith/kubectl-1.14.3-0-linux-amd64-debian-9.tar.gz && \
    tar -zxf /tmp/bitnami/pkg/cache/kubectl-1.14.3-0-linux-amd64-debian-9.tar.gz -P --transform 's|^[^/]*/files|/opt/bitnami|' --wildcards '*/files' && \
    rm -rf /tmp/bitnami/pkg/cache/kubectl-1.14.3-0-linux-amd64-debian-9.tar.gz
RUN pip3 install --no-cache-dir jinja2
RUN chmod +x /opt/bitnami/kubectl/bin/kubectl

ENV PATH="/opt/bitnami/kubectl/bin:$PATH"

USER 1001
